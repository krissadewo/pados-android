package com.dailycode.pados.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dailycode.pados.ImageLoader;
import com.dailycode.pados.activity.R;
import com.dailycode.pados.entity.Book;

public class BookSearchAdapter extends BaseAdapter {

	private ArrayList<Book> data;
	private static LayoutInflater inflater = null;
	public ImageLoader imageLoader;

	public BookSearchAdapter(Activity activity, ArrayList<Book> data) {
		this.data = data;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.imageLoader = new ImageLoader(activity.getApplicationContext());
	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.book_list, null);

		TextView title = (TextView) vi.findViewById(R.id.textViewTitle); // title

		// ImageView thumbImage = (ImageView) vi.findViewById(R.id.list_image); // thumb image

		//HashMap<String, String> book = new HashMap<String, String>();
		Book book = data.get(position);
		vi.setId(book.getId());
		// Setting all values in listview
		title.setText(book.getTitle());
		// imageLoader.DisplayImage(song.get(CustomizedListView.KEY_THUMB_URL), thumb_image);
		return vi;
	}
}