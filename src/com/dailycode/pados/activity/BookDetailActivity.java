package com.dailycode.pados.activity;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.dailycode.pados.App;
import com.dailycode.pados.JSONParser;
import com.dailycode.pados.entity.Book;

public class BookDetailActivity extends Activity {

	private JSONArray jsonArray = null;
	private ProgressDialog progressDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.book_detail);
		Intent intent = this.getIntent();

		Object[] params = new Object[] { (Integer) intent.getIntExtra(App.TAG_ID, 0) };

		progressDialog = ProgressDialog.show(this, "Please Wait", "Loading...");
		new BookDetailTask().execute(params);

		/*
		 * TextView textViewDateRetain = (TextView) findViewById(R.id.textViewDateRetain);
		 * textViewDateRetain.setText(intent.getStringExtra(App.TAG_DATE_RETAIN));
		 */
	}

	class BookDetailTask extends AsyncTask<Object, Integer, Void> {
		private Book book;

		@Override
		protected Void doInBackground(Object... params) {
			Log.d("doInBackground", "doInBackground");
			JSONParser parser = new JSONParser();
			String urlStr = App.getBaseUrl(BookDetailActivity.this) + "/pados-rest/book/id/" + params[0];
			try {
				URL url = new URL(urlStr);
				URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());

				if (parser.isConnected(new URL(App.getBaseUrl(BookDetailActivity.this)))) {
					// getting JSON string from URL
					//JSONObject jsonObject = parser.getJSONFromUrl(uri.toURL());
					jsonArray = parser.getJSONArrayFromUrl(uri.toURL());
					try {
						if (jsonArray != null) {
							//jsonArray = jsonObject.getJSONArray(App.TAG_BOOKS);
							JSONObject c = jsonArray.getJSONObject(0);
							String status = "";
							if (c.getString(App.TAG_STATUS).equals(App.BookStatus.AVAILABLE.toString())) {
								status = "Tersedia";
							} else if (c.getString(App.TAG_STATUS).equals(App.BookStatus.LOOSE.toString())) {
								status = "Hilang";
							} else if (c.getString(App.TAG_STATUS).equals(App.BookStatus.RETAIN.toString())) {
								status = "Dipinjam";
							} else if (c.getString(App.TAG_STATUS).equals(App.BookStatus.UNKNOWN.toString())) {
								status = "Tidak Diketahui";
							}

							book = new Book();
							book.setCode(c.getString(App.TAG_CODE));
							book.setIsbn(c.getString(App.TAG_ISBN));
							book.setWriter(c.getString(App.TAG_WRITER));
							book.setPublisher(c.getString(App.TAG_PUBLISHER));
							book.setStatus(status);
						}

					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Log.d("onPostExecute", "onPostExecute");
		
			TextView textViewCode = (TextView) findViewById(R.id.textViewCode);
			textViewCode.setText(book.getCode());

			TextView textViewISBN = (TextView) findViewById(R.id.textViewWriter);
			textViewISBN.setText(book.getWriter());

			TextView textViewPublisher = (TextView) findViewById(R.id.textViewPublisher);
			textViewPublisher.setText(book.getPublisher());

			TextView textViewStatus = (TextView) findViewById(R.id.textViewStatus);
			textViewStatus.setText(book.getStatus());

			progressDialog.dismiss();
		}

	}

}
