package com.dailycode.pados.activity;

import com.dailycode.pados.App;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class SettingActivity extends Activity {

	private EditText editTextServerAddress;
	private EditText editTextPortAddress;
	private Button button;
	private AlertDialog.Builder aBuilder;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);

		editTextServerAddress = (EditText) findViewById(R.id.editTextServerAddress);
		editTextPortAddress = (EditText) findViewById(R.id.editTextPortAddress);
		button = (Button) findViewById(R.id.btnSave);

		if (App.getServerAddress(this) != null) {
			editTextServerAddress.setText(App.getServerAddress(this));
		}

		if (App.getPortAddress(this) != null) {
			editTextPortAddress.setText(App.getPortAddress(this));
		}

		aBuilder = new AlertDialog.Builder(this);
		aBuilder.setTitle("Information");

		button.setOnClickListener(new OnClickListener() {

			@SuppressLint("CommitPrefEdits")
			@Override
			public void onClick(View arg0) {
				SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SettingActivity.this);
				SharedPreferences.Editor editor = preferences.edit();
				editor.putString(App.SERVER_ADDRESS, editTextServerAddress.getText().toString());
				editor.putString(App.PORT_ADDRESS, editTextPortAddress.getText().toString());
				if (editor.commit()) {
					aBuilder.setMessage("Perubahan telah disimpan");

					aBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							startActivity(new Intent(SettingActivity.this, MainActivity.class));
							arg0.dismiss();
						}
					});

					aBuilder.show();
				}
			}

		});
	}
}