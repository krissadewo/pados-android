package com.dailycode.pados.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class MainActivity extends TabActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		TabHost tabHost = getTabHost();

		TabSpec tabInfo = tabHost.newTabSpec("INFO");
		tabInfo.setIndicator("INFO", getResources().getDrawable(R.drawable.icon_info_tab));
		Intent intentInfo = new Intent(this, BookInfoActivity.class);
		tabInfo.setContent(intentInfo);

		TabSpec tabSearch = tabHost.newTabSpec("SEARCH");
		// setting Title and Icon for the Tab
		tabSearch.setIndicator("SEARCH", getResources().getDrawable(R.drawable.icon_search_tab));
		Intent intentSearch = new Intent(this, BookSearchActivity.class);
		tabSearch.setContent(intentSearch);

		TabSpec tabSetting = tabHost.newTabSpec("SETTING");
		tabSetting.setIndicator("SETTING", getResources().getDrawable(R.drawable.icon_setting_tab));
		Intent intentSetting = new Intent(this, SettingActivity.class);
		tabSetting.setContent(intentSetting);

		// Adding all TabSpec to TabHost
		tabHost.addTab(tabInfo); // Adding photos tab
		tabHost.addTab(tabSearch); // Adding songs tab
		// tabHost.addTab(tabSetting); // Adding videos tab

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.toString().equals("ABOUT")) {
			startActivity(new Intent(this, AboutActivity.class));
		} else if (item.toString().equals("SETTING")) {
			startActivity(new Intent(this, SettingActivity.class));
		}
		return true;
	}

}
