package com.dailycode.pados.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

public class AboutActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
		
		String about = "<h3>Pados For Android</h3>" +
				"<p align='justify'>Pados For Android adalah aplikasi pelengkap untuk aplikasi Pados. Aplikasi ini " +
				"digunakan untuk melakukan pencarian dan memberikan informasi dari status buku terkini. Lakukan setting terlebih" +
				"dahulu untuk dapat menggunkan aplikasi ini.</p><br>dailycode.app";
		
		TextView textView = (TextView) findViewById(R.id.textViewAbout);
		textView.setText(Html.fromHtml(about));
	}

	
}
