package com.dailycode.pados.activity;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

import com.dailycode.pados.App;
import com.dailycode.pados.JSONParser;
import com.dailycode.pados.adapter.BookSearchAdapter;
import com.dailycode.pados.entity.Book;

public class BookSearchActivity extends Activity {

	private ListView listView;
	private EditText editText;
	private ArrayList<Book> books = new ArrayList<Book>();
	private JSONArray jsonArray = null;
	private ProgressDialog progressDialog;
	private AlertDialog.Builder aBuilder;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.book_search);
		
		listView = (ListView) findViewById(R.id.listViewSearch);
		editText = (EditText) findViewById(R.id.editTextSearch);

		aBuilder = new AlertDialog.Builder(this);
		aBuilder.setTitle("Information");
		editText.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// If the event is a key-down event on the "enter" button
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
					progressDialog = ProgressDialog.show(BookSearchActivity.this, "Please Wait", "Loading...");
					new BookTask().execute();
					return true;
				}

				return false;
			}
		});

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent bookActivity = new Intent(BookSearchActivity.this, BookDetailActivity.class);
				bookActivity.putExtra(App.TAG_ID, view.getId());
				startActivity(bookActivity);
			}
		});

	}

	class BookTask extends AsyncTask<Object, Integer, Void> {

		private boolean isConnected = true;

		@Override
		protected Void doInBackground(Object... arg0) {
			Log.d("doInBackground", "doInBackground");
			books.clear();
			JSONParser parser = new JSONParser();
			String urlStr = App.getBaseUrl(BookSearchActivity.this) + "/pados-rest/book/title/" + editText.getText().toString();
			try {
				URL url = new URL(urlStr);
				URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());

				if (parser.isConnected(new URL(App.getBaseUrl(BookSearchActivity.this)))) {
					// getting JSON string from URL
					//JSONObject jsonObject = parser.getJSONFromUrl(uri.toURL());
					jsonArray = parser.getJSONArrayFromUrl(uri.toURL());
					try {
						if (jsonArray != null) {
							//jsonArray = jsonObject.getJSONArray(App.TAG_BOOKS);
							for (int i = 0; i < jsonArray.length(); i++) {
								JSONObject c = jsonArray.getJSONObject(i);
								Book book = new Book();
								book.setId(Integer.valueOf(c.getString(App.TAG_ID)));
								book.setTitle(c.getString(App.TAG_TITLE));							
								books.add(book);
							}
						}

					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else {
					isConnected = false;
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}

			if (!isConnected) {
				aBuilder.setMessage("Koneksi bermasalah");
			} else if (books.size() == 0 && isConnected) {
				aBuilder.setMessage("Buku tidak ditemukan");
			} else {
				aBuilder.setMessage("Ditemukan " + books.size() + " buah buku");
			}

			return null;
		}

		protected void onProgressUpdate(Integer... progress) {
			//Log.d("progress", Integer.toString(progress[0]));
		}

		protected void onPreExecute() {
			super.onPreExecute();
			//Log.d("onPreExecute", "onPreExecute");
		}

		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			//Log.d("onPostExecute", "onPostExecute");

			BookSearchAdapter adapter = new BookSearchAdapter(BookSearchActivity.this, books);
			listView.setAdapter(adapter);
			adapter.notifyDataSetChanged();

			aBuilder.setPositiveButton("OK", new OnClickListener() {

				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					arg0.dismiss();
				}
			});
			AlertDialog alertDialog = aBuilder.create();
			alertDialog.show();

			progressDialog.dismiss();
		}

	}

}
