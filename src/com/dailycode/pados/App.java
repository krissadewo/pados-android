package com.dailycode.pados;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class App {

	// JSON Node names
	public static final String TAG_ID = "id";
	public static final String TAG_ISBN = "isbn";
	public static final String TAG_CODE = "code";
	public static final String TAG_TITLE = "title";
	public static final String TAG_PUBLISH_DATE = "publishDate";
	public static final String TAG_PUBLISHER = "publisher";
	public static final String TAG_WRITER = "writer";
	public static final String TAG_DATE_RETAIN= "dateRetain";
	public static final String TAG_STATUS = "bookStatus";
	public static final String TAG_BOOKS = "book";
	public static final String SERVER_ADDRESS = "SERVER_ADDRESS";
	public static final String PORT_ADDRESS = "PORT_ADDRES";

	public enum BookStatus {
		ALL, AVAILABLE, RETAIN, LOOSE, UNKNOWN;
	}

	public static String getServerAddress(Activity activity) {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(activity);
		return settings.getString(SERVER_ADDRESS, "");
	}

	public static String getPortAddress(Activity activity) {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(activity);
		return settings.getString(PORT_ADDRESS, "");
	}

	public static String getBaseUrl(Activity activity) {
		return "http://" + getServerAddress(activity) + ":" + getPortAddress(activity);
	}
}
